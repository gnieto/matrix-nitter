use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
    str::FromStr,
};

use anyhow::Result;
use async_recursion::async_recursion;
use kuchiki::traits::TendrilSink;
use matrix_sdk::{
    ruma::{events::room::message::RoomMessageEventContent, exports::http::Uri, OwnedMxcUri},
    Client,
};
use reqwest::StatusCode;
use rss::{Channel, Item};
use serde::{Deserialize, Serialize};

use crate::config::Config;

#[derive(Clone)]
pub struct NitterClient {
    client: reqwest::Client,
    base: String,
}

impl NitterClient {
    pub fn new(base: &str) -> anyhow::Result<Self> {
        let client = reqwest::ClientBuilder::new().build()?;

        Ok(NitterClient {
            client,
            base: base.to_string(),
        })
    }

    pub fn link_for_user(&self, user: &TwitterUser) -> anyhow::Result<Uri> {
        let uri = Uri::from_str(&self.base)?;

        let uri = Uri::builder()
            .scheme(
                uri.scheme()
                    .ok_or_else(|| anyhow::anyhow!("missing scheme"))?
                    .clone(),
            )
            .authority(
                uri.authority()
                    .ok_or_else(|| anyhow::anyhow!("missing authority"))?
                    .clone(),
            )
            .path_and_query(format!("/{}", user.without_prefix()))
            .build()?;

        Ok(uri)
    }

    pub async fn profile(&self, user: &str) -> Result<Option<Profile>> {
        let url = format!("{}/{}/rss", self.base, user);
        tracing::info!(url = url.as_str(), "Profile user");
        let response = self.client.get(url).send().await?;
        if response.status() == StatusCode::NOT_FOUND {
            return Ok(None);
        }
        let body = response.text().await?;
        let channel = Channel::read_from(std::io::BufReader::new(body.as_bytes()))?;
        let image = channel.image();

        if image.is_none() {
            return Ok(None);
        }

        let title = image.and_then(|i| i.title().split('/').next().map(|name| name.to_string()));

        let profile = Profile {
            name: title,
            image: image.map(|i| i.url().to_owned()),
        };

        Ok(Some(profile))
    }

    pub async fn timeline(
        &self,
        config: &Config,
        user: &TwitterUser,
        from: Option<String>,
    ) -> Result<Option<Timeline>> {
        let builder = BuildingTimeline::default();

        let timeline = self
            .internal_recursive_timeline(config, user, from, builder)
            .await?
            .map(|builder| {
                tracing::debug!("Timeline size: {}", builder.tweets.len());

                Timeline {
                    head: builder.tweets.into_iter().rev().collect(),
                    pinned: builder.pinned,
                }
            });

        Ok(timeline)
    }

    pub async fn download_image(&self, url: &str) -> Result<ImageContent> {
        let image_response = self.client.get(url).send().await?;
        let content_type = image_response
            .headers()
            .get("content-type")
            .and_then(|value| value.to_str().ok())
            .map(|value| value.to_string())
            .unwrap_or_else(|| "image/png".to_string());
        let bytes = image_response.bytes().await?;

        Ok(ImageContent {
            content: bytes.to_vec(),
            content_type,
        })
    }

    #[async_recursion]
    async fn internal_recursive_timeline(
        &self,
        config: &Config,
        user: &TwitterUser,
        from: Option<String>,
        mut timeline: BuildingTimeline,
    ) -> Result<Option<BuildingTimeline>> {
        let url = format!("{}/{}/rss", self.base, user.without_prefix());
        let mut request = self.client.get(url);

        if let Some(min_id) = timeline.latest_min_id {
            tracing::debug!(?user, "Cursor at {}", min_id);
            request = request.query(&[("cursor", min_id)]);
        }

        let response = request.send().await?;

        if response.status() == StatusCode::NOT_FOUND {
            return Ok(None);
        }

        let min_id = response
            .headers()
            .get("min-id")
            .and_then(|header| header.to_str().ok())
            .map(|header| header.to_string());

        let body = response.text().await?;
        let channel = Channel::read_from(std::io::BufReader::new(body.as_bytes()))?;
        let mut pinned = None;
        let mut found_tweet = false;

        let items: Vec<Tweet> = channel
            .items()
            .iter()
            .filter_map(|item| match Tweet::try_from(item) {
                Ok(tweet) => Some(tweet),
                Err(error) => {
                    tracing::error!(?error, "could not convert item to tweet");
                    None
                }
            })
            .filter(|tweet| {
                let is_pinned = tweet.is_pinned_tweet();
                pinned = Some(tweet.clone());

                !is_pinned
            })
            .take_while(|tweet| match &from {
                Some(latest) => {
                    found_tweet = true;
                    &tweet.guid != latest
                }
                None => true,
            })
            .collect();

        timeline.pinned = pinned;
        timeline.latest_min_id = min_id;
        timeline.tweets.extend_from_slice(&items);
        timeline.requests += 1;

        let found_target_tweet = from.is_some() && found_tweet;
        let tweet_amount_under_limit =
            timeline.tweets.len() < config.timeline_backfill_elements as usize;
        let requests_under_limit = timeline.requests < config.timeline_backfill_requests;

        if !found_target_tweet && tweet_amount_under_limit && requests_under_limit {
            timeline = self.internal_recursive_timeline(config, user, from, timeline).await?
                .ok_or_else(|| anyhow::anyhow!("Recursive function can not return a not found if this function has not already"))?;
        }

        Ok(Some(timeline))
    }
}

#[derive(PartialEq, Eq, Clone, Default, Debug, Serialize, Deserialize)]
pub struct Profile {
    name: Option<String>,
    image: Option<String>,
}

impl Profile {
    pub async fn image_content(
        &self,
        nitter: NitterClient,
    ) -> anyhow::Result<Option<ImageContent>> {
        match &self.image {
            None => Ok(None),
            Some(url) => {
                let image_content = nitter.download_image(&url).await?;
                Ok(Some(image_content))
            }
        }
    }

    pub fn name(&self) -> Option<&String> {
        self.name.as_ref()
    }
}

#[derive(Debug)]
pub struct Timeline {
    pub head: Vec<Tweet>,
    pub pinned: Option<Tweet>,
}

#[derive(Default)]
struct BuildingTimeline {
    requests: u32,
    tweets: Vec<Tweet>,
    pinned: Option<Tweet>,
    latest_min_id: Option<String>,
}

#[derive(Clone, Debug)]
pub struct Tweet {
    pub guid: String,
    pub body: String,
    pub body_html: String,
    creator: TwitterUser,
}

impl Tweet {
    pub fn is_pinned_tweet(&self) -> bool {
        self.body.starts_with("Pinned: ") && !self.body_html.contains("Pinned")
    }

    pub fn creator(&self) -> &TwitterUser {
        &self.creator
    }

    pub fn is_retweet(&self, owner: &TwitterUser) -> bool {
        self.creator() != owner
    }

    pub async fn to_matrix_message(
        mut self,
        nitter_client: &NitterClient,
        matrix_client: &Client,
        user: &TwitterUser,
    ) -> anyhow::Result<RoomMessageEventContent> {
        self.mark_as_retweet(user, nitter_client);
        self.body_html =
            Self::replace_images(&self.body_html, nitter_client, matrix_client).await?;
        let room_message = RoomMessageEventContent::text_html(self.body, self.body_html);

        Ok(room_message)
    }

    fn mark_as_retweet(&mut self, owner: &TwitterUser, nitter_client: &NitterClient) {
        if !self.is_retweet(owner) {
            return;
        }

        match nitter_client.link_for_user(owner) {
            Err(error) => {
                tracing::error!(?error, "Unexpected error while creating link for user");
            }
            Ok(link) => {
                self.body_html = format!(
                    "<p><b>Retweeted (<a href=\"{}\">{}</a>)</b>[<a href=\"{}\">Original</a>]</p>{}",
                    link, self.creator, self.guid, self.body_html
                );
            }
        }
    }

    async fn replace_images(
        content: &str,
        nitter_client: &NitterClient,
        client: &Client,
    ) -> anyhow::Result<String> {
        let images_to_fetch = extract_images_to_fetch(content)?;
        let mut nitter_matrix_image_map =
            copy_images_from_nitter_to_matrix(images_to_fetch, nitter_client, client).await?;

        let parser = kuchiki::parse_html();
        let document = parser.one(content);

        for css_match in document
            .select("img")
            .map_err(|_| anyhow::anyhow!("could not find any image"))?
        {
            let as_node = css_match.as_node();
            let element = as_node
                .as_element()
                .ok_or_else(|| anyhow::anyhow!("expected an element"))?;

            let mut attributes = element.attributes.borrow_mut();

            if let Some(src) = attributes.get_mut("src") {
                if let Some(mxc) = nitter_matrix_image_map.remove(src) {
                    *src = mxc;
                }
            }
        }

        Ok(document.to_string())
    }
}

async fn copy_images_from_nitter_to_matrix(
    images_to_fetch: HashSet<String>,
    nitter_client: &NitterClient,
    client: &Client,
) -> Result<HashMap<String, String>, anyhow::Error> {
    let mut images: HashMap<String, String> = HashMap::new();

    for source in images_to_fetch {
        let image = nitter_client.download_image(source.as_str()).await?;
        let matrix_mxc = image.upload(client).await?;

        images.insert(source, matrix_mxc.to_string());
    }

    Ok(images)
}

fn extract_images_to_fetch(content: &str) -> Result<HashSet<String>, anyhow::Error> {
    let mut images_to_fetch: HashSet<String> = HashSet::new();

    let parser = kuchiki::parse_html();
    let document = parser.one(content);

    for css_match in document
        .select("img")
        .map_err(|_| anyhow::anyhow!("could not find any image"))?
    {
        let as_node = css_match.as_node();
        let element = as_node
            .as_element()
            .ok_or_else(|| anyhow::anyhow!("expected an element"))?;

        let attributes = element.attributes.borrow();

        if let Some(src) = attributes.get("src") {
            images_to_fetch.insert(src.to_string());
        }
    }

    Ok(images_to_fetch)
}

impl TryFrom<&Item> for Tweet {
    type Error = anyhow::Error;

    fn try_from(item: &Item) -> Result<Self, Self::Error> {
        let guid = item
            .guid()
            .map(|guid| guid.value().to_string())
            .ok_or_else(|| anyhow::anyhow!("missing guid"))?;

        let raw = item
            .title()
            .ok_or_else(|| anyhow::anyhow!("missing title"))?;

        let html = item
            .description()
            .ok_or_else(|| anyhow::anyhow!("missing description"))?;

        let extension_creator = item
            .extensions()
            .get("dc")
            .and_then(|dc| dc.get("creator"))
            .and_then(|creator_ext| creator_ext.get(0))
            .and_then(|extension| extension.value())
            .map(|creator| creator.to_owned());

        let dublin_creator = item
            .dublin_core_ext()
            .and_then(|ext| ext.creators().first())
            .map(|creator| creator.to_string());

        let creator = dublin_creator
            .or(extension_creator)
            .ok_or_else(|| anyhow::anyhow!("missing creator {:?}", item))?;

        Ok(Tweet {
            guid,
            body: raw.to_string(),
            body_html: html.to_string(),
            creator: TwitterUser::from(creator.as_str()),
        })
    }
}

#[derive(Serialize, Deserialize, Eq, Ord, PartialOrd, Hash, Clone, Debug)]
#[serde(from = "&str")]
pub struct TwitterUser(String);

impl TwitterUser {
    pub fn without_prefix(&self) -> &str {
        self.0.trim_start_matches('@')
    }
}

impl PartialEq for TwitterUser {
    fn eq(&self, other: &Self) -> bool {
        self.without_prefix() == other.without_prefix()
    }
}

impl From<&str> for TwitterUser {
    fn from(user: &str) -> Self {
        if user.starts_with('@') {
            Self(user.to_string())
        } else {
            Self(format!("@{}", user))
        }
    }
}

impl Display for TwitterUser {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

pub struct ImageContent {
    content: Vec<u8>,
    content_type: String,
}

impl ImageContent {
    pub async fn upload(&self, client: &Client) -> Result<OwnedMxcUri> {
        let mime = mime::Mime::from_str(self.content_type.as_str())?;
        let response = client.media().upload(&mime, self.content.clone()).await?;

        Ok(response.content_uri)
    }

    pub async fn upload_as_avatar(&self, client: &Client) -> Result<()> {
        let mime = mime::Mime::from_str(self.content_type.as_str())?;
        client
            .account()
            .upload_avatar(&mime, self.content.clone())
            .await?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use rss::Channel;

    use crate::nitter::{NitterClient, TwitterUser};

    use super::Tweet;

    #[test]
    fn retweet_mapping() {
        let content = r#"
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
    <channel>
        <item>
            <title>
            RT by @accountA: Content
            </title>
            <dc:creator>@accountB</dc:creator>
            <description>Content</description>
            <pubDate>Tue, 20 Dec 2022 12:00:15 GMT</pubDate>
            <guid>
            http://localhost:8081/accountB/status/11111111111111111#m
            </guid>
            <link>
            http://localhost:8081/accountB/status/11111111111111111#m
            </link>
        </item>
    </channel>
</rss>
        "#;
        let nitter_client = NitterClient::new("http://localhost:8081").unwrap();

        let channel = Channel::read_from(std::io::BufReader::new(content.as_bytes())).unwrap();
        let item = &channel.items()[0];
        let mut tweet = Tweet::try_from(item).unwrap();
        tweet.mark_as_retweet(&TwitterUser::from("@accountA"), &nitter_client);

        assert!(tweet.body_html.contains("Retweeted"));
    }

    #[test]
    fn deserialize_twitter_user() {
        let serialized = r#""user""#;
        let twitter: TwitterUser = serde_json::from_str(serialized).unwrap();

        assert_eq!("@user", twitter.0);
    }
}
