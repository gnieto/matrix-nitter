use std::{collections::HashSet, path::PathBuf, sync::Arc, time::SystemTime};

use anyhow::Result;
use matrix_sdk::ruma::{OwnedRoomId, OwnedUserId, RoomId, UserId};
use rusqlite::Connection;
use serde::{Deserialize, Serialize};

use crate::{
    config::Config,
    nitter::{Profile, TwitterUser},
};

#[derive(Serialize, Deserialize, Debug)]
pub struct Account {
    twitter_handle: TwitterUser,
    matrix_user: OwnedUserId,
    last_update: std::time::SystemTime,
    last_guid: Option<String>,
    #[deprecated]
    room: OwnedRoomId,
    #[serde(default)]
    profile: AccountProfile,
    sync_token: Option<String>,
    #[serde(default)]
    rooms: HashSet<OwnedRoomId>,
}

impl Account {
    pub fn new(handle: TwitterUser, matrix_user: OwnedUserId, room: OwnedRoomId) -> Self {
        let mut rooms = HashSet::new();
        rooms.insert(room.clone());

        Self {
            twitter_handle: handle,
            matrix_user,
            last_update: SystemTime::now(),
            last_guid: None,
            room,
            profile: Default::default(),
            sync_token: None,
            rooms,
        }
    }

    /// Migrate from old format to the new one. This can be removed in the next breaking version
    pub fn migrate_rooms(mut self) -> Self {
        let mut rooms = HashSet::new();
        rooms.insert(self.room.clone());
        self.rooms = rooms;

        self
    }

    pub fn set_latest_guid(&mut self, guid: String) {
        self.last_guid = Some(guid);
        self.last_update = SystemTime::now();
    }

    pub fn set_sync_token(&mut self, token: String) {
        self.sync_token = Some(token);
    }

    pub fn latest_guid(&self) -> Option<&String> {
        self.last_guid.as_ref()
    }

    pub fn sync_token(&self) -> Option<&String> {
        self.sync_token.as_ref()
    }

    pub fn twitter_handle(&self) -> &TwitterUser {
        &self.twitter_handle
    }

    pub fn matrix_user(&self) -> &UserId {
        &self.matrix_user
    }

    pub fn room(&self) -> &RoomId {
        &self.room
    }

    pub fn profile(&self) -> &AccountProfile {
        &self.profile
    }

    pub fn update_profile(&mut self, profile: AccountProfile) {
        self.profile = profile;
    }

    pub fn rooms(&self) -> &HashSet<OwnedRoomId> {
        &self.rooms
    }

    pub fn track_room(&mut self, room_id: &RoomId) {
        self.rooms.insert(room_id.to_owned());
    }

    pub fn untrack_room(&mut self, room_id: &RoomId) {
        self.rooms.remove(room_id);
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct AccountProfile {
    latest_update: SystemTime,
    profile: Option<Profile>,
}

impl Default for AccountProfile {
    fn default() -> Self {
        Self {
            latest_update: SystemTime::now(),
            profile: Default::default(),
        }
    }
}

impl AccountProfile {
    /// Updates the profile if the given profile is different from the current one. Returns true if profile was changed.
    pub fn update(&mut self, update: SystemTime, profile: Option<Profile>) -> bool {
        self.latest_update = update;

        // Do not update the profile if no profile is given, but we had one stored.
        // Nitter is returning a 404 some times even if the profile do exist.
        if profile.is_none() && self.profile.is_some() {
            return false;
        }

        if self.profile == profile {
            tracing::info!(
                "Profile {:?} is equal to the persisted one. Previous {:?}",
                profile,
                self.profile
            );

            return false;
        }

        tracing::info!(
            "Profile {:?} is NOT equal to the persisted one. Previous {:?}",
            profile,
            self.profile
        );
        self.profile = profile;

        true
    }

    pub fn should_update(&self, current_time: &SystemTime, config: &Config) -> bool {
        *current_time
            >= self
                .latest_update
                .checked_add(config.profile_check_frequency)
                .unwrap_or(self.latest_update)
    }
}

#[async_trait::async_trait]
pub trait Database: Send + Sync {
    async fn load_accounts(&self) -> Result<Vec<Account>>;
    async fn load_account_by_twitter(&self, user: &TwitterUser) -> Result<Option<Account>>;
    async fn store_account(&self, account: &Account) -> Result<()>;
}

#[derive(Clone)]
pub struct SqliteDatabase {
    db: Arc<tokio::sync::Mutex<SqliteDbInternal>>,
}

#[async_trait::async_trait]
impl Database for SqliteDatabase {
    async fn load_accounts(&self) -> Result<Vec<Account>> {
        let db = self.db.lock().await;
        let mut stmt = (*db).connection.prepare("SELECT data FROM accounts")?;
        let accounts = stmt
            .query_map([], |row| {
                let data: Account = serde_json::from_str(&row.get::<_, String>(0)?)
                    .map_err(|_| rusqlite::Error::ExecuteReturnedResults)?;

                Ok(data)
            })?
            .collect::<std::result::Result<Vec<_>, _>>()?;

        Ok(accounts)
    }

    async fn load_account_by_twitter(&self, user: &TwitterUser) -> Result<Option<Account>> {
        let db = self.db.lock().await;
        let mut stmt = (*db)
            .connection
            .prepare("SELECT data FROM accounts WHERE twitter_handle = ?")?;
        let account = stmt
            .query_map([&user.to_string()], |row| {
                let data: Account = serde_json::from_str(&row.get::<_, String>(0)?)
                    .map_err(|_| rusqlite::Error::ExecuteReturnedResults)?;

                Ok(data)
            })?
            .next()
            .transpose()?;

        Ok(account)
    }

    async fn store_account(&self, account: &Account) -> Result<()> {
        let twitter = account.twitter_handle().to_string();
        let data = serde_json::to_string(account)?;
        let insert = r#"
            INSERT INTO accounts (twitter_handle, data)
            VALUES (?1, ?2)
            ON CONFLICT DO UPDATE SET data = ?2 
        "#;

        let db = self.db.lock().await;
        (*db).connection.execute(insert, &[&twitter, &data])?;

        Ok(())
    }
}

impl SqliteDatabase {
    pub fn new(config: &Config) -> anyhow::Result<Self> {
        let mut path = PathBuf::from(config.db.clone());
        path.push("matrix_nitter.db");
        let internal = SqliteDbInternal {
            connection: Connection::open(&path)?,
        };

        let create_table_query = r#"
            CREATE TABLE IF NOT EXISTS accounts (
                twitter_handle TEXT PRIMARY KEY,
                data TEXT
            )
        "#;

        internal.connection.execute(create_table_query, [])?;

        Ok(Self {
            db: Arc::new(tokio::sync::Mutex::new(internal)),
        })
    }

    pub async fn import(&mut self, sled: &SledDatabase) -> anyhow::Result<()> {
        let sqlite_accounts = self.load_accounts().await?;

        // If import has already ran, do not run it again
        if sqlite_accounts.len() > 0 {
            return Ok(());
        }
        
        let accounts = sled.load_accounts()?;

        for account in accounts {
            self.store_account(&account).await?;
        }

        Ok(())
    }
}

struct SqliteDbInternal {
    connection: rusqlite::Connection,
}

#[derive(Clone)]
pub struct SledDatabase {
    db: sled::Db,
}

impl SledDatabase {
    pub fn new(config: &Config) -> Result<Self> {
        let db = sled::open(&config.db)?;

        Ok(Self { db })
    }

    pub fn load_accounts(&self) -> Result<Vec<Account>> {
        let tree = self.db.open_tree("accounts")?;

        let accounts = tree
            .iter()
            .map(|tuple| {
                tuple
                    .map_err(anyhow::Error::from)
                    .map(|tuple| tuple.1)
                    .and_then(|value| serde_json::from_slice(&value).map_err(anyhow::Error::from))
            })
            .collect::<Result<Vec<Account>>>()?;

        Ok(accounts)
    }

    pub fn load_account_by_twitter(&self, user: &TwitterUser) -> Result<Option<Account>> {
        let tree = self.db.open_tree("accounts")?;

        let account = tree
            .iter()
            .map(|tuple| {
                tuple
                    .map_err(anyhow::Error::from)
                    .map(|tuple| tuple.1)
                    .and_then(|value| {
                        serde_json::from_slice::<Account>(&value).map_err(anyhow::Error::from)
                    })
            })
            .find(|account| match account {
                Ok(account) => account.twitter_handle() == user,
                Err(_) => false,
            })
            .transpose();

        account
    }

    pub fn store_account(&self, account: &Account) -> Result<()> {
        let tree = self.db.open_tree("accounts")?;
        let key = format!("{}#{}", account.twitter_handle(), account.room());
        tree.insert(key, serde_json::to_vec(&account)?)?;

        tree.flush()?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::time::SystemTime;

    use matrix_sdk::ruma::{room_id, user_id};

    use crate::{db::AccountProfile, nitter::TwitterUser};

    use super::Account;

    #[test]
    fn migrate_account() {
        let room_id = room_id!("!id:localhost:8000");
        let user = user_id!("@user:localhost:8000");
        let old_account = Account {
            twitter_handle: TwitterUser::from("@user"),
            matrix_user: user.to_owned(),
            last_update: SystemTime::now(),
            last_guid: Some("test".to_string()),
            room: room_id.to_owned(),
            profile: AccountProfile::default(),
            sync_token: None,
            rooms: Default::default(),
        };
        let new_account = old_account.migrate_rooms();

        assert_eq!(1, new_account.rooms.len());
        assert!(new_account.rooms.contains(room_id));
    }
}
