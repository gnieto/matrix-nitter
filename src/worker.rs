use std::{
    collections::HashMap,
    sync::Arc,
    time::{Duration, SystemTime},
};

use matrix_sdk::{
    config::SyncSettings,
    ruma::{OwnedRoomId, OwnedTransactionId, RoomId},
};
use rand::{thread_rng, Rng};
use tokio::sync::{mpsc, Mutex};

use crate::{db::Account, nitter::TwitterUser, Context};

#[derive(Debug)]
pub enum Message {
    TrackRoom(OwnedRoomId),
    UntrackRoom(OwnedRoomId),
    NitterTimeline,
}

#[derive(Clone)]
pub struct WorkerState {
    workers: Arc<Mutex<HashMap<TwitterUser, tokio::sync::mpsc::Sender<Message>>>>,
}

impl WorkerState {
    pub fn new() -> Self {
        WorkerState {
            workers: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub async fn start_workers(&self, context: Context) -> anyhow::Result<()> {
        let following = context.db.load_accounts()?;

        for account in following {
            let account = account.migrate_rooms();
            if !account.rooms().is_empty() {
                self.spawn_worker(context.clone(), account.twitter_handle().clone())
                    .await;
            }
        }

        Ok(())
    }

    pub async fn spawn_worker(&self, context: Context, user: TwitterUser) {
        let mut guard = self.workers.lock().await;
        let exists = guard.contains_key(&user);

        if !exists {
            let (tx, rx) = mpsc::channel::<Message>(100);
            tokio::spawn(worker_control_loop(rx, tx.clone(), context, user.clone()));
            guard.insert(user, tx);
        }
    }

    pub async fn send_message(&self, user: &TwitterUser, message: Message) -> anyhow::Result<()> {
        let guard = self.workers.lock().await;
        tracing::info!(?user, ?message, "Sending message");

        if let Some(tx) = guard.get(&user) {
            tx.send(message).await?;
        }

        Ok(())
    }
}

async fn worker_control_loop(
    mut rx: mpsc::Receiver<Message>,
    tx: mpsc::Sender<Message>,
    context: Context,
    user: TwitterUser,
) -> anyhow::Result<()> {
    tracing::debug!(?user, "Starting control loop",);

    tokio::spawn(schedule_timeline(context.clone(), user.clone(), tx.clone()));
    while let Some(msg) = rx.recv().await {
        if let Err(error) = process_message(msg, &context, &user).await {
            tracing::error!(?user, ?error, "Error processing message",)
        }
    }

    Ok(())
}

async fn process_message(
    msg: Message,
    context: &Context,
    user: &TwitterUser,
) -> anyhow::Result<()> {
    let mut account = context
        .db
        .load_account_by_twitter(user)?
        .ok_or(anyhow::anyhow!("could not find target account"))?;

    tracing::info!(?account, ?msg, "Processing message");

    match msg {
        Message::TrackRoom(room) => {
            account.track_room(&room);
        }
        Message::UntrackRoom(room) => {
            account.untrack_room(&room);
        }
        Message::NitterTimeline => internal_worker_control_loop(&context, &mut account).await?,
    }

    tracing::info!(?user, "Store account");

    context.db.store_account(&account)?;

    Ok(())
}

async fn schedule_timeline(context: Context, user: TwitterUser, tx: mpsc::Sender<Message>) {
    loop {
        tracing::info!("Sending timeline event");

        if let Err(error) = tx.try_send(Message::NitterTimeline) {
            tracing::error!(?user, ?error, "could not send to message to user channel",);
        }

        let next_frequency = fuzzy_frequency(context.config.frequency, 5);
        tokio::time::sleep(next_frequency).await;
    }
}

async fn internal_worker_control_loop(
    context: &Context,
    account: &mut Account,
) -> anyhow::Result<()> {
    if account.rooms().is_empty() {
        return Ok(());
    }

    let user = context
        .appservice
        .user(Some(account.matrix_user().localpart()))
        .await?;

    let sync_settings = SyncSettings::default();
    let _result = user.sync_once(sync_settings).await?;

    let timeline = context
        .nitter
        .timeline(
            &context.config,
            account.twitter_handle(),
            account.latest_guid().cloned(),
        )
        .await?;

    if let Some(timeline) = timeline {
        tracing::info!(?account, ?timeline, "Timeline");

        for tweet in timeline.head.into_iter() {
            let latest_guid = tweet.guid.clone();
            let content = tweet
                .to_matrix_message(&context.nitter, &user, account.twitter_handle())
                .await?;

            tracing::info!(?account, ?content, "Content message");
            let mut sent = false;
            for room_id in account.rooms() {
                tracing::info!(?account, ?room_id, ?content, "Room id");

                if let Some(room) = user.get_room(&room_id) {
                    sent = true;
                    let transaction_id = transaction_id(&latest_guid, room_id);
                    tracing::info!(
                        ?account,
                        ?room_id,
                        ?content,
                        ?transaction_id,
                        "Sending message"
                    );
                    let _ = room.send(content.clone(), Some(&transaction_id)).await?;
                } else {
                    tracing::info!(?account, ?room_id, "NOT JOINED TO ROOM");
                }
            }

            if !sent {
                tracing::warn!("could not find any joined room");
                return Err(anyhow::anyhow!("could not find any joined room"));
            }

            account.set_latest_guid(latest_guid);
        }
    }

    let current_time = SystemTime::now();
    if account
        .profile()
        .should_update(&current_time, context.config.as_ref())
    {
        let profile = context
            .nitter
            .profile(account.twitter_handle().without_prefix())
            .await?;
        let mut account_profile = account.profile().to_owned();
        if account_profile.update(current_time, profile.clone()) {
            tracing::info!(?account, "Update profile");

            if let Some(profile) = &profile {
                let image_content = profile.image_content(context.nitter.clone()).await?;
                if let Some(image) = image_content {
                    image.upload_as_avatar(&user).await?;
                }

                if let Some(name) = profile.name() {
                    user.account().set_display_name(Some(&name)).await?;
                }
            }
        }

        account.update_profile(account_profile);
    }

    Ok(())
}

/// Generates a unique transaction id with the given guid and room id
fn transaction_id(guid: &str, room_id: &RoomId) -> OwnedTransactionId {
    let digest = md5::compute(format!("{}{}", guid, room_id));

    OwnedTransactionId::from(format!("{:x}", digest))
}

fn fuzzy_frequency(base: Duration, fuzziness_secs: i32) -> Duration {
    let num: i32 = thread_rng().gen_range(-fuzziness_secs..fuzziness_secs);
    let abs = std::time::Duration::from_secs(num.unsigned_abs() as u64);

    if num < 0 {
        base.saturating_sub(abs)
    } else {
        base.saturating_add(abs)
    }
}
