use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Config {
    #[serde(default = "load_default_db_path")]
    pub db: String,

    #[serde(default = "default_frequency", with = "serde_humanize_rs")]
    pub frequency: std::time::Duration,

    pub nitter_instance: String,

    pub homeserver_name: String,

    pub homeserver_url: String,

    pub timeline_backfill_elements: u32,

    pub timeline_backfill_requests: u32,

    #[serde(default = "default_profile_frequency", with = "serde_humanize_rs")]
    pub profile_check_frequency: std::time::Duration,
}

fn load_default_db_path() -> String {
    let mut dirs = dirs::data_dir().unwrap();
    dirs.push("matrix_nitter");
    let _ = std::fs::create_dir_all(&dirs);

    tracing::debug!("Using path {:?} for database", dirs.as_os_str());

    dirs.to_str().unwrap().to_string()
}

fn default_frequency() -> std::time::Duration {
    std::time::Duration::from_secs(120)
}

fn default_profile_frequency() -> std::time::Duration {
    std::time::Duration::from_secs(3600)
}

pub fn load_config() -> anyhow::Result<Config> {
    let cfg = ::config::Config::builder()
        .add_source(config::File::with_name("Settings"))
        .add_source(config::Environment::with_prefix("MATRIX_NITTER"))
        .build()?
        .try_deserialize()?;

    Ok(cfg)
}
