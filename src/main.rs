pub(crate) mod config;
pub(crate) mod db;
pub(crate) mod nitter;
pub(crate) mod worker;

use db::SqliteDatabase;
use nitter::TwitterUser;
use std::sync::Arc;
use worker::WorkerState;

use crate::config::load_config;
use crate::config::Config;
use crate::worker::Message;
use anyhow::Result;
use db::{Account, SledDatabase};
use matrix_sdk_appservice::{
    matrix_sdk::{
        event_handler::Ctx,
        room::Room,
        ruma::{
            events::room::member::{MembershipState, OriginalSyncRoomMemberEvent},
            UserId,
        },
        Client,
    },
    AppService, AppServiceRegistration,
};
use nitter::NitterClient;
use regex::Regex;
use tracing::trace;

async fn handle_room_member(
    context: Context,
    room: Room,
    event: OriginalSyncRoomMemberEvent,
) -> Result<()> {
    let appservice = context.appservice.clone();
    if !appservice.user_id_is_in_namespace(&event.state_key) {
        trace!("not an appservice user: {}", event.state_key);
        return Ok(());
    }

    let user_id = UserId::parse(event.state_key.as_str())?;
    create_user(&context.appservice, &user_id).await?;
    let client = appservice.user(Some(user_id.localpart())).await?;

    let nitter_user = extract_nitter_user(appservice.registration(), user_id.as_str())?
        .ok_or_else(|| anyhow::anyhow!("could not find a matching nitter/twitter user"))?
        .to_string();
    tracing::info!(
        user = user_id.as_str(),
        translated = nitter_user.as_str(),
        "Translated user",
    );
    let user = TwitterUser::from(nitter_user.as_str());

    if context.db.load_account_by_twitter(&user)?.is_none() {
        let account = Account::new(user.clone(), user_id, room.room_id().to_owned());
        context.db.store_account(&account)?;
    }

    context
        .worker_state
        .spawn_worker(context.clone(), user.clone())
        .await;

    tracing::info!(?event, "Handle room member");

    match event.content.membership {
        MembershipState::Invite => {
            client.join_room_by_id(room.room_id()).await?;
            context
                .worker_state
                .send_message(&user, Message::TrackRoom(room.room_id().to_owned()))
                .await?;
        }
        MembershipState::Leave => {
            context
                .worker_state
                .send_message(&user, Message::UntrackRoom(room.room_id().to_owned()))
                .await?;
        }
        _ => (),
    }

    Ok(())
}

async fn create_user(appservice: &AppService, user_id: &UserId) -> Result<()> {
    if let Err(e) = appservice.register_user(user_id.localpart(), None).await {
        tracing::debug!(
            user = user_id.to_string().as_str(),
            err = e.to_string().as_str(),
            "ignoring error registering user as it has been already registered",
        );
    }

    Ok(())
}

pub fn extract_nitter_user<'a>(
    registration: &AppServiceRegistration,
    user: &'a str,
) -> Result<Option<&'a str>> {
    for namespace in &registration.namespaces.users {
        let regex = Regex::new(&namespace.regex)?;
        let captures = regex.captures(user);
        if let Some(captures) = captures {
            let full_id = captures.get(0);
            let nitter = captures.get(1);

            if let Some(nitter) = nitter {
                return Ok(Some(nitter.as_str()));
            }

            if let Some(m) = full_id {
                return Ok(Some(m.as_str()));
            }
        }

        if regex.is_match(user) {
            return Ok(Some(user));
        }
    }

    Ok(None)
}

#[derive(Clone)]
pub struct Context {
    appservice: AppService,
    config: Arc<Config>,
    nitter: NitterClient,
    db: SledDatabase,
    worker_state: WorkerState,
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::fmt::init();
    let config = load_config()?;
    let db = SledDatabase::new(&config)?;
    let mut sqlite = SqliteDatabase::new(&config)?;
    sqlite.import(&db).await?;

    let registration = AppServiceRegistration::try_from_yaml_file("registration.yaml")?;
    let client_builder = Client::builder();

    let appservice = AppService::builder(
        config.homeserver_url.parse()?,
        config.homeserver_name.parse()?,
        registration,
    )
    .client_builder(client_builder)
    .build()
    .await?;

    let nitter_client = NitterClient::new(&config.nitter_instance)?;
    let worker_state = WorkerState::new();
    let context = Context {
        appservice: appservice.clone(),
        config: Arc::new(config),
        nitter: nitter_client,
        db,
        worker_state,
    };
    context.worker_state.start_workers(context.clone()).await?;
    let user = appservice.user(None).await?;
    user.add_event_handler_context(context);
    user.add_event_handler(
        move |event: OriginalSyncRoomMemberEvent, room: Room, Ctx(appservice): Ctx<Context>| {
            handle_room_member(appservice, room, event)
        },
    );

    let (host, port) = appservice.registration().get_host_and_port()?;
    appservice.run(host, port).await?;

    Ok(())
}
