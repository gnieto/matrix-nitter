FROM clux/muslrust
RUN mkdir /source
WORKDIR /source
COPY ./Cargo.toml /source
COPY ./Cargo.lock /source
COPY ./src /source/src
RUN cargo build --verbose --release
RUN strip /source/target/x86_64-unknown-linux-musl/release/matrix-nitter

FROM alpine:latest
RUN apk --no-cache add ca-certificates
RUN mkdir -p /opt/matrix-nitter
COPY --from=0 /source/target/x86_64-unknown-linux-musl/release/matrix-nitter /opt/matrix-nitter
WORKDIR /opt/matrix-nitter
CMD ./matrix-nitter
